%global date    %(date +%%Y%%m%%d)
%global shortcommit SCOMMITHASH 

Name:    git-lab-porcelain 
Version: 0 
Release: %{date}git%{shortcommit}%{?dist}
Summary: Git porcelain for working with gitlab 
BuildArch: noarch

Requires: python3-gitlab
Requires: python3-GitPython
Requires: python3-pycurl
Requires: python3-tabulate
Requires: curl
Requires: python3-argcomplete

License: GPLv2
URL:     https://gitlab.com/nhorman/git-lab-porcelain 

Source0: %{name}-%{shortcommit}.tar.gz

%description
A porcelain for git to facilitate command line creation/listing/editing and
reviewing of merge requests in gitlab

%prep
%autosetup -n git-lab-porcelain-%{shortcommit}


%build
# nothing to do here

%install
mkdir -p $RPM_BUILD_ROOT/%{_bindir}/
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1/
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/%{name}/filters
install -m 0755 git-lab $RPM_BUILD_ROOT/%{_bindir}/git-lab
install -m 0644 man1/* $RPM_BUILD_ROOT/%{_mandir}/man1/
install -m 0644 contrib/filters/rhklready.py $RPM_BUILD_ROOT/%{_datadir}/%{name}/filters/rhklready.py
activate-global-python-argcomplete

%files
%{_bindir}/*
%{_mandir}/man1/*
%{_datadir}/%{name}/filters

%changelog
* Tue Oct 22 2019 Neil Horman <nhorman@redhat.com> - 0-1
- Initial release
