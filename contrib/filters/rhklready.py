import sys
import gitlab
import git
import json
import tempfile
import os
import requests
import rpdb

def get_bzs(commit):
    mlines = commit.message.split('\n')
    for l in mlines:
        if l.startswith('BZ:'):
            l = l[3:]
            l = l.replace(',', ' ')
            return l.split()
    raise Exception('No BZs Found in commit ' + str(commit))

def extract_files(commit):
    filelist = []
    diffstats = commit.stats.files
    for k in diffstats:
        filelist.append(k)
    return filelist

def get_mr_ci_state(lab, mrobj):
    return mrobj.pipelines()

def get_mr_bz_state(repo, lab, project, mrobj):
    bzs = []
    # create a temp string to name this temporary fetch branch
    try:
        _, lbranch = tempfile.mkstemp() 
        lbranch = os.path.basename(lbranch)
        repo.remotes.origin.fetch('merge-requests/'+str(mrobj.iid)+'/head:'+lbranch)
        for c in mrobj.commits():
            commit = repo.commit(c.id)
            for bz in get_bzs(commit):
                files = extract_files(commit)
                bzs.append({'commit': str(commit), 'bz': bz, 'files': files})

        for bz in bzs:
            buginputs = {
                    'package': 'kernel',
                    'namespace': 'rpms',
                    'ref': 'refs/heads/'+mrobj.target_branch,
                    'commits': [{
                        'hexsha': bz['commit'],
                        'files': bz['files'],
                        'resolved': [str(bz['bz'])],
                        'related': [],
                        'reverted': [],
                    }]}

            bugresults = requests.post("https://10.19.208.80/lookaside/gitbz-query.cgi", json=buginputs, verify=False)
            bz['dist-git-logs'] = bugresults.json()
    except Exception as e:
        sys.stderr.write("Error encountered collecting bugzillas: %s\n" % str(e))
    finally:
        repo.delete_head(lbranch, "-D")
    return bzs

def get_mr_approvals(mrobj):
    approvals = mrobj.approvals.get()
    return approvals.attributes

def check_ci_ready(cidata):
    # Just check the most recent ci run to see if it passed
    if cidata[0]['status'] == 'success':
        return True
    return False

def check_bz_ready(bzdata):
    # look at every bz in the list, and for each one, check dist-git-logs
    # and look to see if any entry in the result field says fail, we're not
    # ready
    if len(bzdata) == 0:
        return False
    for bz in bzdata:
        if bz['dist-git-logs']['result'] == 'fail':
            return False
    return True 

def check_approval_ready(approvedata):
    # check to make sure the approvals left field is zero
    if approvedata['approvals_left'] == 0:
        return True
    return False

def check_if_merge_ready(mrdata):
    if not check_ci_ready(mrdata['ci']):
       return False
    if not check_bz_ready(mrdata['bz']):
        return False
    if not check_approval_ready(mrdata['approvals']):
        return False 
    return True

def check_merge_requests(repo, lab, project, mriids):
    mrresults = []
    for mr in mriids:
        sys.stderr.write("Collecting data for MR %s\n" % mr)
        result = {'project': project.id}
        mrobj = project.mergerequests.get(mr)
        result['mriid'] = mrobj.iid
        result['data'] = {}
        result['data']['ci'] = get_mr_ci_state(lab, mrobj)
        result['data']['bz'] = get_mr_bz_state(repo, lab, project, mrobj)
        result['data']['approvals'] = get_mr_approvals(mrobj)
        result['mergeready'] = check_if_merge_ready(result['data'])
        mrresults.append(result)
    return mrresults

def runfilter(repo, lab, input):
    outputdata = None 
    for p in input:
        project = lab.projects.get(p)
        sys.stderr.write("Checking mrs for project %s : %s\n" % (p, project.name))
        mroutput = check_merge_requests(repo, lab, project, input[p])
        if outputdata == None:
            outputdata = mroutput
        else:
            outputdata.append(mroutput) 
    return outputdata

def filterinfo():
    info = ("rhklready filters merge request lists based on Red Hat bugzilla\n"
           "readiness policy, by inspecting the following aspects of an MR\n"
           "* Latest CI results\n"
           "* BZ readiness collection from commits basd on dist-git policy\n"
           "* Number of MR approvals as configured in the project\n"
           "\n"
           "Output of the filter is defined as the followng json\n"
           "[\n"
           " {\n"
           "  .project /*project number from gitlab*/\n"
           "  .mriid /*MR iid from gitlab\n"
           "  .mergeready /*boolean to declare weather this MR passes merge policy*/\n"
           "  .data = { /* dictionary of various data sources used to determine mergeready value*/ }\n"
           " }\n"
           "]\n")
    return info
